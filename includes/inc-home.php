<p>Hi. My name is Perry, and I create websites and digital products. </br>
I've been connecting to the world via web technologies since 1998 - when the crunch-crunch-jangle-jangle of German 56K dialup sang a young me to sleep.</p>
<p>
I’ve lived and traveled a lot since then, picking up skills, good stories, a couple pets, an even more muddled accent... and have settled in NE Portland with my family.</p>
<p>Thanks to some <a href="portfolio.php">great jobs and volunteer projects</a>, I’ve had the opportunity to work on a wide variety of exciting development. I’ve also branched out into project management, where I continue to love working with people just as much as I enjoy creating code and exploring new ideas. I’ve always liked training new employees and interns and get a lot out of teams with a wide range of skill levels and backgrounds.</p>
<p>
So I'd love to hear from you if you have an awesome project where I can bring or develop my <a href="skillset.php">skills</a>.
If you’ve seen me speak and want to book me to present at your event, conference or training, <a href="contact.php">get in touch</a> too!  Have fun looking around on my site, its design is all my own. </p>