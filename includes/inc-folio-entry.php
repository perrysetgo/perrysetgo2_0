<?php
require_once('includes/pdo-server.php');
$stmt = $pdo->query("SELECT * FROM projects");
$count =''; /*reset the counter*/
$image=1; /*reset the image number*/
$modal =''; /*reset the modal number*/

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

$count =$row['has_img_desc'];
$modal = $row['modal_counter'];?>
<!--start folio <?php echo $row['proj_title']?> item-->

<?php
echo '<div id="folio-item" class="element'.' '.
      $row['skill1'].' '.$row['skill2'].' '.
      $row['skill3'].' '.$row['skill4'].' '.
      $row['skill5'].' '.$row['skill6'].' '.
      $row['skill7'].' '.$row['skill8'].'">'
      ;?>
            <ul class="grid cs-style-5">
                 <li>
                     <figure>
                     	<?php
                     	$image=1;

                     	echo '<img src="images/portfolio/'.$row['img_prefix'].'-'. $image.$row['img_suffix'].'" alt="'.$row['alt_text1'].'">'; ?>
                         <figcaption>
                             <span class="name"><?php echo $row['proj_title']?> </span>// <span class="number"><?php echo $row['year']?></span>
                                 <h3>C-Factor <?php echo $row['c_factor']?></h3>
                                 <a href="#" data-reveal-id="myModal<?php echo $modal;?>">Open Project</a>
                         </figcaption>
                     </figure>
                 </li>
             </ul>

     <div id="myModal<?php echo $modal;?>" class="reveal-modal small" data-reveal>
         <h4><?php echo $row['proj_title']?></h4>
 <?php echo '<img src="images/portfolio/'.$row['img_prefix'].'-'. $image.$row['img_suffix'].'" alt="'.$row['alt_text1'].'">';
 ; ?>
         <p><?php echo $row['description1'];?> </p>
     </br>

            <?php
			if ($image < $count){ //load the second image and descriptionif there is one
                $image++;
				echo '<img src="images/portfolio/'.$row['img_prefix'].'-'. $image.$row['img_suffix'].'" alt="'.$row['alt_text2'].'">';
				echo '<p>'.$row['description2'].'</p>';

			}

            if($image < $count){ //load the third image and description if there is one
                $image++;
                echo '<img src="images/portfolio/'.$row['img_prefix'].'-'. $image.$row['img_suffix'].'" alt="'.$row['alt_text3'].'">';
                echo '<p>'.$row['description3'].'</p>';
                $image=1;
                }

?>


     </br>
         <a class="close-reveal-modal">&#215;</a>
     </div>
 </div>
 <!--end <?php echo $row['proj_title']?> folio item-->
 <?php
}

?>
