<div class="columns large-2 medium-8 small-12 end">
  <section id="options">
    <div class="option-combo">
      <p id="options_title">Make it work for you:</p>
      <ul id="sort" class="option-set clearfix" data-option-key="sortBy">
        <li><a href="#sortBy=original-order" data-option-value="original-order" class="selected" data>C-Factor</a><span data-tooltip aria-haspopup="true" class="has-tip" title="You know, every developer would like you to check out their coolest projects first.">?</span></li>
        <li><a href="#sortBy=number" data-option-value="number">Chronological</a></li>
        <li><a href="#sortBy=name" data-option-value="name">Alphabetical</a></li>
      </ul>
      <p id="options_title">Sort direction</p>
      <ul id="sort-direction" class="option-set clearfix" data-option-key="sortAscending">
        <li><a href="#sortAscending=true" data-option-value="true" class="selected">sort up</a
          ><span data-tooltip aria-haspopup="true" class="has-tip" title="Alphabetical A-Z, or Z-A? Coolest first or last? Use these to switch.">?</span></li>
        <li><a href="#sortAscending=false" data-option-value="false">sort down</a></li>
      </ul>
      <!-- begin sorting links -->
      <ul id="filters" class="option-set clearfix" data-option-key="filter">
        <li><a href="#filter" data-option-value="*" class="selected">show all</a></li>
        <li><a id="buttonsend" class="t" >
            <span class="t">show filters</span>
            <span class="t" style="display:none">less -</span>
          </a>
          <span data-tooltip aria-haspopup="true" class="has-tip" title="I hid some filters under here to allow you to customize.">?</span></li>
      </ul>
        <div id="hidden">
          <ul id="filters" class="option-set clearfix" data-option-key="filter">            <!-- begin filters -->
            <li><a href="#filter" data-option-value=".html">HTML</a></li>
            <li><a href="#filter" data-option-value=".css">CSS</a></li>
            <li><a href="#filter" data-option-value=".php">PHP</a></li>
            <li><a href="#filter" data-option-value=".javas">JS</a></li>
            <li><a href="#filter" data-option-value=".photoshop">PhotoShop</a></li>
            <li><a href="#filter" data-option-value=".wordpress">WordPress</a></li>
            <li><a href="#filter" data-option-value=".jquery">JQuery</a></li>
<!--             <h5>&nbsp;Motivation:</h5>
            <li><a href="#filter" data-option-value=".forprofit">For Profit</a></li>
            <li><a href="#filter" data-option-value=".nonprofit">Non Profit</a></li>
            <h5>&nbsp;Languages:</h5>
            <li><a href="#filter" data-option-value=".german">German</a></li>
            <li><a href="#filter" data-option-value=".english">English</a></li> -->
          </ul>
        </div>  <!--close div hidden-->
        <!-- end filter links -->
    </div>
  </section>
</div>
<div class="columns panel large-9 medium-8 small-12 end">
I'm excited to show you my portfolio - it's my favorite thing I have worked on lately! I have a diverse work history and I wanted to create a way to showcase my portfolio that didn't force my projects into a specific order. There is a lot going on here - try hovering over tooltips such as these to find more specific information about how you can quickly get around, or just play.
<span data-tooltip aria-haspopup="true" class="has-tip" title="These tooltips will help you out!">?</span>
</div>
<div class="columns large-10 medium-8 small-12">
  <div id="container" class="group">
  <!--start database include -->
  <?php
  require_once('includes/pdo-server.php');
  $stmt = $pdo->query("SELECT * FROM projects");
  $count =''; /*reset the counter*/
  $image=1; /*reset the image number*/
  $modal =''; /*reset the modal number*/

  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

  $count =$row['has_img_desc'];
  $modal = $row['modal_counter'];?>
  <!--start folio <?php echo $row['proj_title']?> item-->

  <?php
  echo '<div id="folio-item" class="element'.' '.
        $row['skill1'].' '.$row['skill2'].' '.
        $row['skill3'].' '.$row['skill4'].' '.
        $row['skill5'].' '.$row['skill6'].' '.
        $row['skill7'].' '.$row['skill8'].'">'
        ;?>
              <ul class="grid cs-style-5">
                   <li>
                       <figure>
                       	<?php
                       	$image=1;

                       	echo '<img src="images/portfolio/'.$row['img_prefix'].'-'. $image.$row['img_suffix'].'" alt="'.$row['alt_text1'].'">'; ?>
                           <figcaption>
                               <span class="name"><?php echo $row['proj_title']?> </span>// <span class="number"><?php echo $row['year']?></span>
                                   <h3>C-Factor <?php echo $row['c_factor']?></h3>
                                   <a href="#" data-reveal-id="myModal<?php echo $modal;?>">Open Project</a>
                           </figcaption>
                       </figure>
                   </li>
               </ul>

       <div id="myModal<?php echo $modal;?>" class="reveal-modal small" data-reveal>
           <h4><?php echo $row['proj_title']?></h4>
   <?php echo '<img src="images/portfolio/'.$row['img_prefix'].'-'. $image.$row['img_suffix'].'" alt="'.$row['alt_text1'].'">';
   ; ?>
           <p><?php echo $row['description1'];?> </p>
       </br>

              <?php
  			if ($image < $count){ //load the second image and descriptionif there is one
                  $image++;
  				echo '<img src="images/portfolio/'.$row['img_prefix'].'-'. $image.$row['img_suffix'].'" alt="'.$row['alt_text2'].'">';
  				echo '<p>'.$row['description2'].'</p>';

  			}

              if($image < $count){ //load the third image and description if there is one
                  $image++;
                  echo '<img src="images/portfolio/'.$row['img_prefix'].'-'. $image.$row['img_suffix'].'" alt="'.$row['alt_text3'].'">';
                  echo '<p>'.$row['description3'].'</p>';
                  $image=1;
                  }

  ?>


       </br>
           <a class="close-reveal-modal">&#215;</a>
       </div>
   </div>
   <!--end <?php echo $row['proj_title']?> folio item-->
   <?php
  }

  ?>

  <!--end database include -->
  </div>
</div>
