<dl class="tabs vertical" data-tab>
 <dd class="tab-title active tabheaders"><a href="#panel1">Skill Overview</a></li> 
 <dd class="tab-title"><a href="#panel1a">Project Management</a></dd> 
<dd class="tab-title"><a href="#panel1aa">Training and Leadership</a></dd>
 <dd class="tab-title"><a href="#panel2">Front End Development</a></dd> 
 <dd class="tab-title"><a href="#panel3">Front End Design</a></dd> 
 <dd class="tab-title"><a href="#panel4">Content Management Systems</a></dd>
 <dd class="tab-title"><a href="#panel5">Graphics, Animation and Multi-Media </a></dd>
</dl>  

 <div class="tabs-content"><!--start content panels-->
	 <div class="content active panelpadding" id="panel1">I have been working in tech for over 15 years, and have developed a broad skillbase that allows me to participate in projects on many different levels. Click left to see more about each individual skill area.
	 </div>

	 <div class="content panelpadding" id="panel1a">My experience as a designer and developer allows me o bridge the gap between the two, ensuring that all sides of project teams work together to reach interim and final project goals.
	 </div>

	 <div class="content panelpadding" id="panel1aa"> <p> Companies, projects and project teams are only as good as the training that initiates them and the leadership that sustains them. My ability to communicate with diverse teams allows me to focus disparate groups toward complex goals. </p>
	</div>

	<div class="content panelpadding" id="panel2"> <p>     This is where I got my start! In 1999, I became what was then called "lead HTML" developer, and my speciality was crossbrowser and crossplatform optimization. I learned to write clear, concise valid HTML code from day one. Since then, I have expanded my repertoire of front end dev skills to include fluent CSS and some JavaScript, and am currently expanding my knowledge into SASS and LESS. I frequently integrate scripts based on JQuery in my work, and am excited to learn more about Angular JS.</p> </div> 

	<div class="content panelpadding" id="panel3"> <p>Working in small web startup also exposed me to Front End Design that preclude the actual front end programming. I am well versed in creating front end design techniques with Adobe Photoshop and other graphics tools, while I increasingly using more front end design frameworks such as Foundation 5 by Zurb.</p> </div> 

	<div class="content panelpadding" id="panel4"> <p>  I have been working with content management systems for many years, including WordPress v1.5 Strayhorn! I routinely run Wordpress installations, as well as adding, editing, extending and mending wordpress plugins. Other systems include Drupal, Xcart, and more. </p> </div>

	<div class="content panelpadding" id="panel5"> <p>    Graphics optimization for the web, including creating well-compressed and quick-loading images, is still an important experience to have in a Front End Specialist. I have ample experience optimizing web graphics with Photoshop and Fireworks, as well as basic audio, video and animation skills, including Final Cut Pro.
	</div>

</div><!--end content panels-->