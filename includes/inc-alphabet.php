             <ul id="alphabet" class="show-for-medium-up show-for-landscape">
                <li><span>ACCENTED</span></li>
                <li><span>BRAINY</span></li>
                <li><span>CURIOUS</span></li>
                <li><span>DRIVEN</span></li>
                <li><span>ENERGETIC</span></li>
                <li><span>FRIENDLY</span></li>
                <li><span>GROUNDED</span></li>
                <li><span>HANDS ON</span></li>
                <li><span>IDEALIST</span></li>
                <li><span>JOVIAL</span></li>
                <li><span>KAIZEN</span></li>
                <li><span>LOCALIZED</span></li>
                <li><span>MULTITASKER</span></li>
                <li><span>NAMEKNOWER</span></li>
                <li><span>OUT</span></li>
                <li><span>PRAGMATIC</span></li>
                <li><span>QUIET</span></li>
                <li><span>REALIST</span></li>
                <li><span>SILLY</span></li>
                <li><span>TRUTHTELLER</span></li>
                <li><span>UNAFRAID</span></li>
                <li><span>VERBOSE</span></li>
                <li><span>XCITED</span></li>
                <li><span>WELL-ROUNDED</span></li>
                <li><span>YOUNGATHEART</span></li>
                <li><span>ZIGZAG</span></li>
                </ul>


