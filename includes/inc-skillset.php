  <div id="page-wrap" class="show-for-medium-up">
<!-- start backend column -->
    <div class="info-col">
        <a class="image backend">View Image</a>
        <dl>
          <dt>Backend</dt>
          <dd>I love problems that need to be solved - and work to replace functioning HTML prototypes with smart, efficient PHP scripting.</dd>
          <dt>Tools</dt>
          <dd>PHPMyadmin, Sublime Text 2, PHP Mailer, Git for version control. Some experience administering servers.</dd>
          <dt>Techniques</dt>
          <dd>Form and error handling, includes, simple databases with PDO, loops, basic functions, calculations, randomization, redirects.</dd>
          <dt>Goals</dt>
          <dd>I’m practicing more advanced programming, databases, session handling, object oriented php. I'd like to learn more about Ruby and Ruby on Rails.</dd>
        </dl>
    </div>
<!-- end backend column -->
<!-- start frontend column -->
    <div class="info-col">
        <a class="image frontend">View Image</a>
        <dl>
          <dt>Frontend/UX</dt>
          <dd>I code by hand, but also rely on development interfaces such as Foundation 5.Frontend is my first love!</dd>
          <dt>Tools</dt>
          <dd>Axure RP, Wordpress templating, Photoshop, Wordpress, Wordpress Genius Themes, Foundation 5, Git, Sublime Text 2.</dd>
          <dt>Languages</dt>
          <dd>JQuery, JavaScript, CSS 2/3, HTML 5, </dd>
          <dt>Goals</dt>
          <dd>More on JQuery and other JavaScript libraries, Bootstrap.</dd>
        </dl>
    </div>
  <!-- end frontend column -->
  <!-- end graphics and design column -->
    <div class="info-col">
    	 <a class="image design">View Image</a>
    	 <dl>
    	  <dt id="starter">Design</dt>
    	  <dd>I prototype graphical layouts, usually either in Photoshop or InDesign, then optimize them for web or print processes.</dd>
    	  <dt>Tools</dt>
    	  <dd>Advanced: Adobe Photoshop, Adobe Fireworks, Microsoft Publisher. Intermediate: Adobe InDesign, Adobe Fireworks, Adobe Flash.</dd>
    	  <dt>Techniques</dt>
    	  <dd>Image optimization, page layout, banner ad creation, slicing, image formats, simple animation.</dd>
        <dt>Goals</dt>
      <dd>Advanced CS and photo editing techniques, formal design principles, and typography</dd>
    </div>
    <!-- end graphics and design column -->
		<!-- start project management column -->
    <div class="info-col">
        <a class="image project">View Image</a>
        <dl>
          <dt>Project Management</dt>
          <dd>I'm an experienced web project manager who understands both sides of the equation. I have many years of experience working with diverse teams, and focusing them towards achieving a common goal.</dd>
          <dt>Tools</dt>
          <dd>Basecamp, Freedcamp, Trello, Github, intranets.</dd>
          <dt>Skills</dt>
          <dd>Excellent written and verbal communication skills, approachable and friendly, hard working and ethical, big picture thinker. Nerdy people person, improvement focused.</dd>
          <dt>Goals</dt>
          <dd>Learning more about Agile methodologies, getting my PMP.</dd>
        </dl>
    </div>
    <!-- end project management column -->
		<!-- start training column -->
		<div class="info-col">
     		  <a class="image training">View Image</a>
    			<dl>
    		  <dt>Training</dt>
    		  <dd>I love working with people just as much as I love working with code and ideas. Sharing my knowledge and passion always develops it further. My goal is always to help and inspire good people, get them started or growing, and realize untapped potential.</dd>
    		  <dt>Tools</dt>
    		  <dd>Finding intersecting commonality, shared goals &amp; expectations. Oh, and Powerpoint, Prezi, tap-dancing (j/k).</dd>
    		  <dt>Techniques</dt>
    		  <dd>Focusing and energizing teams.  Fostering collaboration. Reframing weaknesses and adversity as opportunity for growth. Outstanding brainstorming skills.</dd>
          <dt>Goals</dt>
          <dd>More equity building, speaking, training opportunities. Mentorship and sponsorship, eventually!</dd>
    		</dl>
		</div>
		<!-- end training column -->
  </div>
</div>
<div class="row show-for-small-only">
  <div class="columns large-12">
  <ul class="accordion" data-accordion>
  <li class="accordion-navigation">
    <a href="#panel1a">Backend</a>
    <div id="panel1a" class="content">
          <h6>Backend</h6>
          I love problems that need to be solved - and work to replace functioning HTML prototypes with smart, efficient PHP scripting.
          <h6>Tools</h6>
          PHPMyadmin, Sublime Text 2, PHP Mailer, Git for version control. Some experience administering servers.
          <h6>Techniques</h6>
          Form and error handling, includes, simple databases with PDO, loops, basic functions, calculations, randomization, redirects.
         <h6>Goals</h6>
          I’m practicing more advanced programming, databases, session handling, object oriented php. I'd like to learn more about Ruby and Ruby on Rails.
    </div>
  </li>
  <li class="accordion-navigation">
    <a href="#panel2a">Frontend</a>
    <div id="panel2a" class="content">
          <h6>Frontend/UX</h6>
          I code by hand, but also rely on development interfaces such as Foundation 5.Frontend is my first love!
          <h6>Tools</h6>
          Axure RP, Wordpress templating, Photoshop, Wordpress, Wordpress Genius Themes, Foundation 5, Git, Sublime Text 2.
          <h6>Languages</h6>
          JQuery, JavaScript, CSS 2/3, HTML 5, 
          <h6>Goals</h6>
          More on JQuery and other JavaScript libraries, Bootstrap.
    </div>
  </li>
  <li class="accordion-navigation">
    <a href="#panel3a">Design</a>
    <div id="panel3a" class="content">
        <h6>Design</h6>
        I prototype graphical layouts, usually either in Photoshop or InDesign, then optimize them for web or print processes.
        <h6>Tools</h6>
        Advanced: Adobe Photoshop, Adobe Fireworks, Microsoft Publisher. Intermediate: Adobe InDesign, Adobe Fireworks, Adobe Flash.
        <h6>Techniques</h6>
        Image optimization, page layout, banner ad creation, slicing, image formats, simple animation.
        <h6>Goals</h6>
      Advanced CS and photo editing techniques, formal design principles, and typography
    </div>
  </li>
    <li class="accordion-navigation">
    <a href="#panel4a">Project Management</a>
    <div id="panel4a" class="content">
          <h6>Project Management</h6>
          I'm an experienced web project manager who understands both sides of the equation. I have many years of experience working with diverse teams, and focusing them towards achieving a common goal.
          <h6>Tools</h6>
          Basecamp, Freedcamp, Trello, Github, intranets.
          <h6>Skills</h6>
          Excellent written and verbal communication skills, approachable and friendly, hard working and ethical, big picture thinker. Nerdy people person, improvement focused.
          <h6>Goals</h6>
          Learning more about Agile methodologies, getting my PMP.
    </div>
  </li>
      <li class="accordion-navigation">
    <a href="#panel5a">Training</a>
    <div id="panel5a" class="content">
          <h6>Training</h6>
          I love working with people just as much as I love working with code and ideas. Sharing my knowledge and passion always develops it further. My goal is always to help and inspire good people, get them started or growing, and realize untapped potential.
          <h6>Tools</h6>
          Finding intersecting commonality, shared goals &amp; expectations. Oh, and Powerpoint, Prezi, tap-dancing (j/k).
          <h6>Techniques</h6>
          Focusing and energizing teams.  Fostering collaboration. Reframing weaknesses and adversity as opportunity for growth. Outstanding brainstorming skills.
          <h6>Goals</h6>
          More equity building, speaking, training opportunities. Mentorship and sponsorship, eventually!
    </div>
  </li>
</ul>
</div>
</div>