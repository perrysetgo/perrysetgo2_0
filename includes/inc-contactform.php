<?php
require_once("config.php");
$spam = '';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = trim($_POST["name"]);
    $email = trim($_POST["email"]);
    $phone = trim($_POST["phone"]);
    $message = trim($_POST["message"]);

    if (isset ($_POST["check"])){ //check to see if spam prevention is active
    $spam = $_POST["check"];}

    if ($name == "" OR $email == "" OR $message == "" AND $spam != 'yes') {
        $error_message = "<div data-alert class=\"small-12 radius medium-6 large-6 alert-box\">You must specify a value for name, email address, and message. You don't need to enter a phone number.</div>";
    }//someone forgot to fill a required field

    if ($spam == 'yes'){
        $error_message ="<div data-alert class='small-12 medium-6 large-6 alert-box info radius'>I'm not so into spam, though.</br>Would you mind unchecking that box?<a href='#' class='close'>&times;</a></div>";
    }//spam prevention box was checked

    if (!isset($error_message)) {
        foreach( $_POST as $value ){
            if( stripos($value,'Content-Type:') !== FALSE ){
                $error_message = "<div data-alert class=\"small-12 radius medium-6 large-6 alert-box\">There was a problem with the information you entered.</div>";
            } //injection hijack prevention here
        }
    }

    if (!isset($error_message) && $_POST["address"] != "") {
        $error_message = "<div data-alert class=\"small-12 radius medium-6 large-6 alert-box\">Your form submission has an error.</div>";
    } //honepot spam protection here

    require_once(ROOT_PATH . "phpmailer/class.phpmailer.php");
    $mail = new PHPMailer();

    if (!isset($error_message) && !$mail->ValidateAddress($email)){
        $error_message = "You must specify a valid email address.";
    }

    if (!isset($error_message)) {
/**
         * This example shows settings to use when sending via Google's Gmail servers.
         */

        //SMTP needs accurate times, and the PHP time zone MUST be set
        //This should be done in your php.ini, but this is how to do it if you don't have access to that
        date_default_timezone_set('Etc/UTC');

        require 'phpmailer/PHPMailerAutoload.php';

        //Create a new PHPMailer instance
        $mail = new PHPMailer;

        //Tell PHPMailer to use SMTP
        $mail->isSMTP();

        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 2;

        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';

        //Set the hostname of the mail server
        $mail->Host = 'mail.perrysetgo.com';

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 26;

        //Set the encryption system to use - ssl (deprecated) or tls
        //$mail->SMTPSecure = 'tls';

        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = "hello@perrysetgo.com";

        //Password to use for SMTP authentication
        $mail->Password = "!tired06";

        //Set who the message is to be sent from
        $mail->setFrom($email, $name);

        //Set who the message is to be sent to
        $mail->addAddress('lectique@gmail.com', 'Perry');

        //Set the subject line
        $mail->Subject = 'PerrySetGo Contact form message from:'. $name;

        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->isHTML(true);
        //$mail->msgHTML();
        $mail->Body = 'The following info came through:<p>Name: '.$name.'</p><p>Email: '.$email.'</p><p>Phone: '.$phone.'</p><p>Message: '.$message.'</p>';
        //Replace the plain text body with one created manually
        $mail->AltBody = 'This is a plain-text message body';
        if(!$mail->Send()) {
        $error_message = "<div data-alert class=\"small-12 radius medium-6 large-6 alert-box\">There was a problem sending the email: " . $mail->ErrorInfo."</div>";
        } else {  
        header('Location:contactus-thankyou.php');
        exit;

        }

    }
}
    //thank you page redirect follows
     if (isset($_GET["status"]) AND $_GET["status"] == "thanks") { ?>
        <p>Thanks for the email! I&rsquo;ll be in touch shortly!</p> 
<?php } else { 
        if (!isset($error_message)) {
        echo '<div class="small-12 medium-6 large-6 panel">
        Please fill out the form below to get in touch. I\'ll respond as soon as I can!</div>';
        } else {
        echo '<div data-alert class=\"small-12 radius medium-6 large-6 alert-box\">' . $error_message . '</div>';
        }?>
<form method="post">               
    <div class="columns small-12 medium-6 large-6">
        <label for="name" id="reg">Name:</label>
        <input type="text" name="name" id="name" value="<?php if (isset($name)) { echo htmlspecialchars($name); } ?>">
        <label for="email" id="reg">Email:</label>
        <input type="text" name="email" id="email" value="<?php if(isset($email)) { echo htmlspecialchars($email); } ?>">
        <label for="phone" id="reg">Phone:</label>
        <input type="text" name="phone" id="phone" value="<?php if(isset($phone)) { echo htmlspecialchars($phone); } ?>">
        <label for="message" id="reg">Message:</label>
        <textarea name="message" id="message"><?php if (isset($message)) { echo htmlspecialchars($message); } ?></textarea><label for="check">Are you a robot? If yes, leave that ticked if you like.</label><input type="checkbox" name="check" value="yes" checked><span style="display: none;"><label for="address">Address</label>                                <input type="text" name="address" id="address">
        <p>Humans: leave this field blank.</p></span>
        <p><input type="submit" value="Send" class="button small"></p>
    </div>
</form>
<?php } //close it up?>