<!doctype html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PerrySetGo! Welcome to my site</title>
    <link rel="stylesheet" href="css/app.css" />
    <link rel="stylesheet" href="css/extra.css" />
    <link href='http://fonts.googleapis.com/css?family=Ropa+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Karla' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="bower_components/dragula.js/dist/dragula.min.css" type='text/css'>
    </head>
  <body>
    <!-- start header.html -->
    <div class="row">
       <div class="medium-12 large-12 small-12">
         <div class="specialborders">
           <nav class="top-bar " data-topbar role="navigation">
           <ul class="title-area">
             <li class="name"></li>
             <li class="toggle-topbar menu-icon">
               <a href="#" class="specialborders_a">
               <span>MENU</span>
               </a>
             </li>
           </ul>
             <section class="top-bar-section">
               <!-- Left Nav Section -->
               <ul class="left">
                  <li><a href="index.php">home</a></li>
                  <li><a href="portfolio.php">portfolio</a></li>
                  <li><a href="skillset.html">skillset</a></li>
                  <li><a href="contact.html">connect</a></li>
                  <li><a href="blog.html">blog</a></li>
                </ul>
               <!-- end left nav section -->
             </section>
           </nav>
         </div>
       </div>
    </div>
    <!--end header.html -->
    <div class="row">
      <div class="columns panel large-12 medium-12 small-12">
        Below you can see a portfolio of some of my work. When compiling my work, I realized that a one-size-fits-all approach to presenting my work wasn't useful. I've been creating websites and since the late 90's, but also took a long break from being an active developer. During those years I worked as a QA, project manager, and even outside of tech completely, and my recent work has been mostly in Java and Android development and design, as well as working with MVCs such as Ember and Angular.  Because my career spans many different genres and has been unfolding for upward of 15 years now, it made more sense to make something curatable. Make a selection of the things you would like to see from the filters below, and a custom portfolio will be generated for you.
      </div>
    </div>
    <div class="row">
      <br>
      <div class='columns large-12 medium 12 small-12'>
        <div id='left-defaults' class='thingy'>
        <a href="#" id="html" class="button round tiny">HTML</a>
        <a href="#" id="css" class="button round tiny">CSS/SASS</a>
        <a href="#" id="ember" class="button round tiny">Ember JS</a>
        <a href="#" id="java" class="button round tiny">Java</a>
        <a href="#" id="pm" class="button round tiny">Project Management</a>
        <a href="#" id="android" class="button round tiny">Android</a>
        <a href="#" id="sqldb" class="button round tiny">SQL</a>
        <a href="#" id="java" class="button round tiny">Java</a>
        <a href="#" id="photoshop" class="button round tiny">PhotoShop</a>
        <a href="#" id="jquery" class="button round tiny">jQuery</a>
        <a href="#" id="firebase" class="button round tiny">Firebase</a>
        <a href="#" id="api" class="button round tiny">API's</a>
        <a href="#" id="js" class="button round tiny">JavaScript</a>
      </div>
    </div>
    <br>
    <br>
    <div id='right-defaults' class='thingy'>
here

    </div>
          <a href="#" id="generate" class="button round warning">generate!</a>

  <?php include ('getentries.php') ?>
  

  <div class="folio-item ember">i am the ember div</div>



  <!-- start footer html -->

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src='bower_components/dragula.js/dist/dragula.min.js'></script>
<script src='bower_components/dragula.js/example/example.min.js'></script>
<script src='bower_components/isotope/dist/isotope.pkgd.min.js'></script>
<script type="text/javascript">
$(document).ready(function(){
  $('.folio-item').hide();

  // $('.folio').isotope({
  // //options
  // itemSelector: '.folio-item',
  // layoutMode:'fitRows'
  // });

  $("#generate").click(function(event){
  event.preventDefault();

  var idArray=[];

    $("#right-defaults").children().each(function(index){
      idArray.push($(this).attr("id"));
      });
        for (var i=0; i<idArray.length; i++){
        debugger;
        var current = idArray[i];
        current= "." + current;
        $(current).show();
    }

  });
  });

</script>
</body>


</html>
