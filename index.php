<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>PerrySetGo! Welcome to my site</title>
  <link rel="stylesheet" href="css/app.css" />
  <link rel="stylesheet" href="css/extra.css" />
  <link href='http://fonts.googleapis.com/css?family=Ropa+Sans' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Karla' rel='stylesheet' type='text/css'>
</head>
<body>
  <div class="row medium-collapse large-collapse" id="home">
    <div class="columns large-9 medium-9 small-12 small-centered large-centered medium-centered">
      <div class="specialborders small-centered large-centered medium-centered">
        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area">
            <li class="name"></li>
            <li class="toggle-topbar menu-icon"><a href="#" class="specialborders_a"><span>MENU</span></a></li>
          </ul>
          <section class="top-bar-section">
            <!-- Left Nav Section -->
            <ul class="left">
              <li><a data-scroll href="#home">home</a></li>
              <li><a data-scroll href="#speaking">speaking</a></li>
              <li><a data-scroll href="#teaching">teaching</a></li>
              <li><a data-scroll href="#contact">connect</a></li>
              <li><a data-scroll href="#portfolio">dev portfolio</a></li>
              <!-- <li><a href="#">blog</a></li> -->
            </ul>
            <!-- end left nav section -->
          </section>
        </nav>
      </div>
    </div>
  </div>
  <div class="row" id="mainpanel">
    <div class="inner-col columns large-9 medium-9 small-12 small-centered large-centered medium-centered">
      <div class="row medium-collapse large-collapse">
        <div class="columns large-9 medium-9 small-12" id="#home">
          <br> <br>
          <img src="images/triptych.png">
           <p class="panel">
            Hi! My name is Perry, and I am a frontend developer and educator, currently teaching Introduction to Prorgamming, JavaScript, CSS, Java and Android at Epicodus, a vocational "code school" for Programmers in Portland, Oregon. </br>
            I've been connecting to the world via web technologies since 1998 - when the crunch-crunch-jangle-jangle of German 56K dialup sang a young me to sleep. Since then, Ive worked in tech in various forms: As a frontend developer specializng in HTML, CSS and JS programming, as a QA for the video game industry, as a freelance developer working for a Wordpress based shop, a lab assistant for a faculty tech lab at a major college, as a project manager for a webdesign company, and now a educator for a rapidly growing vocational school for programmers!<br>
            To top things off, I've lived in the UK, Germany, and Australia, but have been happy to call Portland home for the last 10 years. I like to draw from all these varied experiences in the presentations I give and the articles I write - I am dedicated to the cause of increasing the headcount of women and minorities in tech and tech-adjacent careers. I'm all about making tech accessible, and dismantling the notion that only Computer Science graduates can succeed in tech. Have a look around, and check out some of my current work or previos projects. If you’ve seen me speak and want to book me to present at your event, conference or training, please get in touch! I'd love to hear more. </p>
          </div>
          <div class="hide-for-small-only medium-2 large-2 columns medium-offset-1 large-offset-1">
            <ul class="alphabet show-for-medium-up show-for-landscape">
              <li><span class="leftAlpha">ACCENTED</span></li>
              <li><span class="leftAlpha">BRAINY</span></li>
              <li><span class="leftAlpha">CURIOUS</span></li>
              <li><span class="leftAlpha">DRIVEN</span></li>
              <li><span class="leftAlpha">ENERGETIC</span></li>
              <li><span class="leftAlpha">FRIENDLY</span></li>
              <li><span class="leftAlpha">GROUNDED</span></li>
              <li><span class="leftAlpha">HANDS ON</span></li>
              <li><span class="leftAlpha">IDEALIST</span></li>
              <li><span class="leftAlpha">JOVIAL</span></li>
              <li><span class="leftAlpha">KAIZEN</span></li>
              <li><span class="leftAlpha">LOCALIZED</span></li>
              <li><span class="leftAlpha">MULTITASKER</span></li>
              <li><span class="leftAlpha">NAMEKNOWER</span></li>
              <li><span class="leftAlpha">OUT</span></li>
              <li><span class="leftAlpha">PRAGMATIC</span></li>
              <li><span class="leftAlpha">QUIET</span></li>
              <li><span class="leftAlpha">REALIST</span></li>
              <li><span class="leftAlpha">SILLY</span></li>
              <li><span class="leftAlpha">TRUTHTELLER</span></li>
              <li><span class="leftAlpha">UNAFRAID</span></li>
              <li><span class="leftAlpha">VERBOSE</span></li>
              <li><span class="leftAlpha">XCITED</span></li>
              <li><span class="leftAlpha">WELL-ROUNDED</span></li>
              <li><span class="leftAlpha">YOUNGATHEART</span></li>
              <li><span class="leftAlpha">ZIGZAG</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row medium-collapse large-collapse">
      <div class="columns small-12 medium-9 large-9 large-centered medium-centered end">
        <span class="up text-center"><a data-scroll href="#home">^</a></span>
      </div>
    </div>
    <section id="speaking">
      <div class="row">
        <div class="inner-col columns speaking-image large-9 medium-9 small-12 small-centered large-centered medium-centered end">
          <h1>saying things out loud in front of people.</h1>
        </div>
      </div>
      <div class="row">
        <div class="inner-col columns large-9 medium-9 small-12 large-centered medium-centered end">
          <div class="row">
            <div class="columns large-3 medium-3 show-for-medium-up">
              <br>
              <a class="twitter-timeline" href="https://twitter.com/perrysetgo" data-widget-id="716822772261847040" data-chrome="nofooter" height="400" data-tweet-limit="4"> @perrysetgo</a>
              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>
            <div class="columns large-8 medium-8 medium-offset-1 large-offset-1 end">
              <br>
              <p>
                Next to teaching and working as a developer, I'm also a vocal member of Portland's tech/advocacy community, and can frequently be found at local Women in Tech and Lesbians Who Tech events. I most recently presented at the Lesbians Who Tech 2016 Summit in San Francisco, where I had the honor (and joy) to present a talk to some of the summit's 1800+ attendees! I will also be presenting at this year's <a href="http://www.act-w.org">ACT-W conference</a> on April 22nd in Portland. <br>
                <br>

                <iframe width="854" height="480" src="https://youtu.be/hFj0xN_0Ygo?list=PLeyuBfm1VLo6N9T2X_pfi1z8QqzjZFp9T&t=97" frameborder="0" allowfullscreen></iframe>
                Generally speaking (geddit) I talk publicly about issues such as diversity in hiring and tech, empowerment for women, LGBTQ minority rights, human rights, equality and more. In my public appearances, I frequently draw from my own journey and struggles as well as my experiences studying cultural anthropology to connect with diverse populations. My goal in public speaking is to encourage others to reflect upon the own journey, to question perceived obstacles, and to find new ways to frame their experiences. Check out <a href="http://lanyrd.com/profile/perrysetgo/">Lanyrd</a> to see up to date information about where I've been speaking, or visit <a href="http://www.slideshare.net/PerryEising/">SlideShare</a> to see my slides.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="row medium-collapse large-collapse">
        <div class="columns small-12 medium-9 large-9 large-centered medium-centered end">
          <span class="up text-center"><a data-scroll href="#home">^</a></span>
        </div>
      </div>
      <section id="teaching">
        <div class="row">
          <div class="inner-col columns teaching-image large-9 medium-9 small-12 small-centered large-centered medium-centered end">
            <h1>making a difference. creating a future.</h1>
          </div>
        </div>
        <div class="row">
          <div class="inner-col columns large-9 medium-9 small-12 large-centered medium-centered end">
            <div class="row">
              <div class="columns large-3 medium-3 small-12">
                <br>
                <br>
                <h4 class="text-center">subject areas I teach:</h4><a href="#" id="loadList" class="button show-for-small"><br>Click to load List</a>
                <ul class="alphabet" id="subjectList">
                  <li class="show-for-medium-up"><span>Version control with Git and GitHub</span></li>
                  <li class="show-for-medium-up"><span>Using the Command Line</span></li>
                  <li class="show-for-medium-up"><span>Sublime text/Atom</span></li>
                  <li class="show-for-medium-up"><span>Agile Methodology Basics</span></li>
                  <li class="show-for-medium-up"><span>Implementing API's </span></li>
                  <li class="show-for-medium-up"><span>Java Basics</span></li>
                    <li class="show-for-medium-up"><span>Android Development &amp; Material Design</span></li>
                    <li class="show-for-medium-up"><span>Android API's</span></li>
                    <li class="show-for-medium-up"><span>Java based Web development, Spark, Velocity</span></li>
                    <li class="show-for-medium-up"><span>SQL Basics (PostGres, SQlite, Sql2o)</span></li>
                    <li class="show-for-medium-up"><span>Gulp Build Tools</span></li>
                    <li class="show-for-medium-up"><span>Bootstrap as a Front End Framework</span></li>
                    <li class="show-for-medium-up"><span>Angular.js</span></li>
                    <li class="show-for-medium-up"><span>Ember.js</span></li>
                    <li class="show-for-medium-up"><span>Introduction to Programming</span></li>
                    <li class="show-for-medium-up"><span>Introduction to Node.js and NPM</span></li>
                    <li class="show-for-medium-up"><span>CSS</span></li>
                    <li class="show-for-medium-up"><span>HTML5</span></li>
                    <li class="show-for-medium-up"><span>jQuery</span></li>
                    <li class="show-for-medium-up"><span>JavaScript programming</span></li>
                    <li class="show-for-medium-up"><span>Object Oriented Programming</span></li>
                    <li class="show-for-medium-up"><span>RESTful Routing</span></li>
                    <li class="show-for-medium-up"><span>BaaS Databases (Firebase, Parse)</span></li>
                    <li class="show-for-medium-up"><span>Test/Behaviour Driven Development:
                    Mocha.js, Chai.js, JUnit and Fluentlenium/Selenium</span></li>
                  </ul>
                <br>
                  </div>
                  <div class="columns large-7 medium-7 medium-offset-1 large-offset-1 end">
                    <p>Teaching at a code school is a dream job. It allows me to bring my professional interests (student development and my love of tech) together in a place where I can positively impact people's lives.<br><br>
                    As a teacher, I am part project manager, part developer, part tech support, and part cheering squad! Of our 170 students, 30 will be my core group at any given time, and they often stay my students from course to course. In an average day, I will respond to approximately 20 requests for help - which results in over 320 requests a month - or over 3800 bugs in a one year period! Additionally, I also spend one on one time with my students every week, giving them personalized feedback on their independent projects. </p><br>
                        <p>
                      My students come from all different backgrounds, races, ages, and genders, and I try hard to create an atmosphere that values them as individuals. In my teaching, I emphasize the value of mutual respect, honest self assessment, hard work, and fun. I try my best to ensure that my students will have the knowledge, tools and experience they need to be successful in their future careers, as well as understanding the importance of making tech more diverse and welcoming to women, and racial, ethnic and gender minorities. I balance my expectations of high standards with ample praise and an approachable, caring attitude. The opinions of my students speak for themselves.</p>
                        </p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="quoteCol columns large-4 medium-4">
                        <blockquote>
                          Perry was really great. I give them an A++++++++ They were always helpful and super knowledgeable about what they were doing. They were very supportive. --2/4/2016
                        </blockquote>
                      </div>
                      <div class="quoteCol columns large-4 medium-4">
                        <blockquote>
                          Perry is the best! Helpful, encouraging, and great at showing us how to tackle problems. --2/4/2016
                        </blockquote>
                      </div>
                      <div class="quoteCol columns large-4 medium-4">
                        <blockquote>
                          Perry is a great teacher - helping move past coding challenges and obstacles without being overbearing. Just giving a nudge in the right direction and letting the student figure it out from there. --2/19/2016
                        </blockquote>
                      </div>
                    </div>
                    <div class="row">
                      <div class="columns small-12 medium-3 large-3 small-centered medium-centered large-centered end">
                        <div class="text-center"><a href="#" id="quoteMore" class="button small">load more...</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row medium-collapse large-collapse">
                <div class="columns small-12 medium-9 large-9 large-centered medium-centered end">
                  <span class="up text-center"><a data-scroll href="#home">^</a></span>
                </div>
              </div>
            </section>
            <section id="contact">
              <div class="row">
                <div class="inner-col columns contact-image large-9 medium-9 small-12 small-centered large-centered medium-centered end">
                  <h1>let's connect.</h1>
                </div>
              </div>
              <div class="row">
                <div class="inner-col columns large-9 medium-9 small-12 large-centered medium-centered end">
                  <div class="row">
                    <div class="columns large-8 medium-8 medium-offset-1 large-offset-1 end">
                      <p>I am excited to connect with you! If you are local to Portland, there is a good chance we might see each other at a local Lesbians Who Tech or Women in Tech event. Come over and say hi if you do!<p>
                        As mentioned above, I speak periodically at conferences - you can find out where I'll be presenting next through my Lanyrd account. Have a conf you want me to present at? Get in touch! <br>
                        If you are a bit further away, no problem. Your best bet is probably to <a href="http://twitter.com/perrysetgo">tweet me</a> and we can take it from there.
                        <br><br>
                        When I'm not working on conference presentations, I sometimes put my thoughts out in blog format - I'm currently experimenting with <a href="https://medium.com/@perrysetgo">Medium</a>, but you can also find me featured on the <a href="http://www.epicodus.com/blog/">Epicodus Blog</a> for now.<br><br>
                        I'm also on other social sites if that's more convenient for you. I'm especially interested in hearing out opportunities to speak or present to your group or company, or to write something for your blog or online feature.
                        <br><br>
                        I look forward to it!</p>
                      </div>
                      <div class="columns large-3 medium-3">
                        <br>
                        You can also connect with me here:
                        <br><br>
                        <ul class="alphabet">
                          <li><a href="http://twitter.com/@perrysetgo"><span>Twitter</span></a></li>
                          <li><a href="https://www.linkedin.com/in/perrysetgo"><span>LinkedIn</span></a></li>
                          <li><a href="http://lanyrd.com/profile/perrysetgo/"><span>Lanyrd</span></a></li>
                          <li><a href="http://www.slideshare.net/PerryEising/"><span>SlideShare</span></a></li>
                          <li><a href="http://twitter.com/@perrysetgo"><span>Github</span></a></li>
                          <li><a href="https://medium.com/@perrysetgo"><span>Medium</span></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row medium-collapse large-collapse">
                  <div class="columns small-12 medium-9 large-9 large-centered medium-centered end">
                    <span class="up text-center"><a data-scroll href="#home">^</a></span>
                  </div>
                </div>
              </section>
            </div>
            <div class="row">
              <div class="columns small-12 medium-9 large-9 small-centered large-centered medium-centered end">
                <p id="socialvert">
                  <a href="https://www.linkedin.com/pub/perry-eising/4a/ba6/a92">
                    <img src="images/perry-linkedin.gif" alt="Work" title="Work" width="36"  height="36">
                  </a>
                  <a href="https://twitter.com/perrysetgo">
                    <img src="images/perry-twitter.gif" alt="Talk" title="Talk" width="36"  height="36">
                  </a>
                  <a href="http://lanyrd.com/profile/perrysetgo/">
                    <img src="images/perry-lanyrd.gif" alt="Meet" title="Meet" width="36"  height="36">
                  </a>
                </p>
              </div>
            </div>
            <!--close main row-->
            <script src="bower_components/jquery/dist/jquery.min.js"></script>
            <script src="bower_components/smooth-scroll/dist/js/smooth-scroll.js"></script>
            <script src="bower_components/foundation/js/foundation.min.js"></script>
            <script src="js/app.js"></script>
            <script>
            smoothScroll.init({
              selector: '[data-scroll]', // Selector for links (must be a valid CSS selector)
              selectorHeader: '[data-scroll-header]', // Selector for fixed headers (must be a valid CSS selector)
              speed: 500, // Integer. How fast to complete the scroll in milliseconds
              easing: 'easeInOutCubic', // Easing pattern to use
              offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
              updateURL: true, // Boolean. If true, update the URL hash on scroll
              callback: function ( anchor, toggle ) {} // Function to run after scrolling
            });

            $(document).ready(function(){
              //quote loading
                var i = 0;
              $('#quoteMore').click(function(event){
                event.preventDefault();
                var quoteArr = [
                  'Perry is supportive and inclusive! --1/7/2016',
                  'Perry is so great! I can tell that they value hard work and are honest and pointed with both criticism and praise. What an outstanding teacher! --2/18/2016',
                  'Perry is a great teacher! Glad to be in their team! --1/21/2016',
                  'My first day, my partner and I ran into a huge problem. Perry set aside their time to troubleshoot what exactly was going on. Apparently another pair was having the same difficulties. It probably took about an hour to finally figure out what it was but got through it! --1/7/2016',
                  'Perry is a great teacher - helping move past coding challenges and obstacles without being overbearing. Just giving a nudge in the right direction and letting the student figure it out from there. --2/19/2016',
                  'Perry is an excellent teacher, and I think they will do an even better job in Java! --1/28/2016',
                  'Continuing the Perry is great: I really appreciated that on my code review they wrote detailed comments on what I did well in it, rather than a simple \"Looks good! Good work!\" That sort of things is encouraging and helps me feel better about my code. Also their daily sweep toward the end of the day for anyone who has questions but didn\'t put in a help queue is really nice.--2/18/2016',
                  'Perry was always around asking if we needed help or if we were stuck. --3/11/2016',
                  'PERRY RULEZ --3/3/2016'];
                var quoteCol = $('.quoteCol');

                debugger;
                //make button have slightly better UX
                if (i <= quoteArr.length ){
                  debugger;
                  for (var counter = 0; counter <= 2; counter++){
                    $(quoteCol[counter]).append('<blockquote>' + quoteArr[i] + '</blockquote>');
                    i++;
                  }
                  return i;
                }
                else{
                  $('#quoteMore').addClass('disabled');
                  return;
                }
              });

                $('#loadList').click(function(event){
                  event.preventDefault();
                    //future - add 5 in one go, show another button to load.
                  $('#subjectList').children().removeClass('show-for-medium-up');
                });
              });
            </script>
          </body>
          </html>
